## 0.2.1

Added ability to disable URL escaping when setting URL Segments/Parameters value

## 0.1.3

Updated Readme and Relinked VIs

## 0.1.2

Removed headers from redirected requests

## 0.1.1

Removed test code from package

## 0.1.0

Initial Release